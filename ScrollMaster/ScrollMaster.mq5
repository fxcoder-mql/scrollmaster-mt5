/*
Copyright 2023 FXcoder

This file is part of ScrollMaster.

ScrollMaster is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ScrollMaster is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ScrollMaster. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "ScrollMaster 2.1. © FXcoder"
#property link      "https://fxcoder.blogspot.com"

/*
Прокрутка текущего графика будет также приводить к прокрутке остальных (несвёрнутых) графиков.
Скрипт отключает автопрокрутку и сдвиг у всех таких графиков и возвращает их на место после выхода.
*/

//#define DEBUG

#include <Generic/ArrayList.mqh>
#include "ScrollMaster-include/chart.mqh"
#include "ScrollMaster-include/math.mqh"
#include "ScrollMaster-include/series.mqh"
#include "ScrollMaster-include/terminal.mqh"
#include "ScrollMaster-include/timer.mqh"


class CScrollMasterChartInfo
{
public:

	long id;
	bool autoscroll;
	bool shift;

	void CScrollMasterChartInfo(const CChart &chart):
		id(chart.id()),
		autoscroll(chart.autoscroll()),
		shift(chart.shift())
	{
	}
};

void OnStart()
{
	// Отключить автопрокрутку и сдвиг у всех несвёрнутых графиков.
	// Запомнить эти параметры для дальнейшего восстановления.

	CArrayList<CScrollMasterChartInfo*> charts_info;

	for (long chart_id = ChartFirst(); chart_id >= 0; chart_id = ChartNext(chart_id) )
	{
		CChart chart(chart_id);

		if (chart.is_object() || chart.is_minimized())
			continue;

		charts_info.Add(new CScrollMasterChartInfo(chart));
		chart.autoscroll(false);
		chart.shift(false);
		chart.redraw();
	}

	// Ждать в цикле, пока пользователь пролистает основной (этот) график,
	// и затем прокрутить остальные графики на это же место.

	const int charts_count = charts_info.Count();

	// Выход по Esc не работает в 4 и иногда не срабатывает в 5, поэтому здесь альтернативный способ выхода - двойное нажатие End
	const uint dbl_press_max_ms = 555;
	const uint dbl_press_min_ms = 100; // против дребезга
	CTimer end_timer(dbl_press_max_ms);
	end_timer.stop();
	int end_press_count = 0;

	while (!IsStopped() && !_terminal.is_escape_key_pressed())
	{
		// check exit
		if (_terminal.is_end_key_pressed())
		{
			if (_math.is_in(end_timer.elapsed(), dbl_press_min_ms, dbl_press_max_ms + 1) || end_timer.is_stopped())
				end_press_count++;

			if (end_press_count >= 2)
				break;

			end_timer.reset();
		}
		else
		{
			if (end_timer.check())
			{
				end_press_count = 0;
				end_timer.stop();
			}
		}

		// update position

		int master_pos = get_chart_pos(_chart);
		int master_bar = master_pos > 0 ? 0 : -master_pos;
		datetime master_time = _series.time(master_bar, false, true);

		for (int i = 0; i < charts_count; i++)
		{
			CScrollMasterChartInfo* info;
			if (!charts_info.TryGetValue(i, info))
				continue;

			CChart chart(info.id);
			if (chart.is_current())
				continue;

			CSeries ser(chart.symbol(), chart.period());

			int pos = get_chart_pos(chart);
			int bar = pos > 0 ? 0 : -pos;
			datetime pos_time = ser.time(bar, false, true);

			if (pos_time != master_time)
			{
				int new_bar = ser.bar_shift(master_time);
				chart.navigate_end(-new_bar);
			}
		}

		// Без этого будет нагрузка на процессор, паузу выбрать по вкусу
		Sleep(33);
	}

	// Восстановить параметры графиков
	for (int i = 0; i < charts_count; i++)
	{
		CScrollMasterChartInfo* saved;
		if (!charts_info.TryGetValue(i, saved))
		{
			Print("No info for chart #", i);
			continue;
		}

		CChart chart(saved.id);
		chart.autoscroll(saved.autoscroll);
		chart.shift(saved.shift);

		if (saved.autoscroll)
			chart.navigate_end(0);

		delete saved;
	}
}

int get_chart_pos(const CChart &chart)
{
	return chart.width_in_bars() - chart.first_visible_bar() - 2;
}
